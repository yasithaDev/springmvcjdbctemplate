package com.oknow.jdbctemplate.Daos;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
//import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.oknow.jdbctemplate.entities.Item;

@Repository
public class ItemDao
{
    @Autowired
    JdbcTemplate jdbcTemplate;
   
    public List<Item> getAllItems(int id_category) throws SQLException
    {
       return jdbcTemplate.query("SELECT * FROM view_item_list where item_is_active = '1'  and id_category = '30' and is_publish='1' and is_approved='1' ;",new BeanPropertyRowMapper<Item>(Item.class));
    }
    
}
