package com.oknow.jdbctemplate.entities;

public class Item
{
    private int id_item;
    private int id_sub_category;
    private String Item_name_english;
    private String Item_name_sinhala;
    private String Item_name_tamil;
    private String Item_short_description;
    private String Item_long_description;
    private String Street_Name;
    private String Latitude;
    private String Longitude;
    private String Item_map_src;
    private String item_image;
    private int item_is_active;
    private int is_publish;
    private int is_approved;
    private int created_id;
    private String publish_date;
    private String item_rating_value;
    private int id_category;
    private String category_name_english;
    private String category_name_sinhala;
    private String category_name_tamil;
    private String category_description;
    private String  category_image;
    private int category_is_active;
    private String sub_category_name_english;
    private String sub_category_name_sinhala;
    private String sub_category_name_tamil;
    private String sub_category_description;
    private int sub_category_is_active;
    private String contact_value_english;
    private String contact_name_type_english;

    public int getId_item() {
        return id_item;
    }
    public void setId_item(int id_item) {
        this.id_item = id_item;
    }
    public String getItem_name_english() {
        return Item_name_english;
    }
    public void setItem_name_english(String item_name_english) {
        Item_name_english = item_name_english;
    }
    public String getItem_short_description() {
        return Item_short_description;
    }
    public void setItem_short_description(String item_short_description) {
        Item_short_description = item_short_description;
    }
    public String getItem_long_description() {
        return Item_long_description;
    }
    public void setItem_long_description(String item_long_description) {
        Item_long_description = item_long_description;
    }
    public String getItem_image() {
        return item_image;
    }
    public void setItem_image(String item_image) {
        this.item_image = item_image;
    }
    public String getLatitude() {
        return Latitude;
    }
    public void setLatitude(String latitude) {
        Latitude = latitude;
    }
    public String getLongitude() {
        return Longitude;
    }
    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public int getId_sub_category() {
        return id_sub_category;
    }

    public void setId_sub_category(int id_sub_category) {
        this.id_sub_category = id_sub_category;
    }
    public int getCreated_id() {
        return created_id;
    }

    public void setCreated_id(int created_id) {
        this.created_id = created_id;
    }
    public int getIs_approved() {
        return is_approved;
    }

    public void setIs_approved(int is_approved) {
        this.is_approved = is_approved;
    }
    public int getIs_publish() {
        return is_publish;
    }

    public void setIs_publish(int is_publish) {
        this.is_publish = is_publish;
    }
    public int getItem_is_active() {
        return item_is_active;
    }

    public void setItem_is_active(int item_is_active) {
        this.item_is_active = item_is_active;
    }

    public String getItem_name_tamil() {
        return Item_name_tamil;
    }

    public void setItem_name_tamil(String item_name_tamil) {
        Item_name_tamil = item_name_tamil;
    }
    public String getStreet_Name() {
        return Street_Name;
    }

    public void setStreet_Name(String street_Name) {
        Street_Name = street_Name;
    }

    public String getContact_name_type_english() {
        return contact_name_type_english;
    }

    public void setContact_name_type_english(String contact_name_type_english) {
        this.contact_name_type_english = contact_name_type_english;
    }
    public String getItem_map_src() {
        return Item_map_src;
    }

    public void setItem_map_src(String item_map_src) {
        Item_map_src = item_map_src;
    }

    public String getItem_name_sinhala() {
        return Item_name_sinhala;
    }

    public void setItem_name_sinhala(String item_name_sinhala) {
        Item_name_sinhala = item_name_sinhala;
    }

    public String getItem_rating_value() {
        return item_rating_value;
    }

    public void setItem_rating_value(String item_rating_value) {
        this.item_rating_value = item_rating_value;
    }
    public String getPublish_date() {
        return publish_date;
    }

    public void setPublish_date(String publish_date) {
        this.publish_date = publish_date;
    }

    public int getId_category() {
        return id_category;
    }

    public void setId_category(int id_category) {
        this.id_category = id_category;
    }

    public int getCategory_is_active() {
        return category_is_active;
    }

    public void setCategory_is_active(int category_is_active) {
        this.category_is_active = category_is_active;
    }
    public String getCategory_image() {
        return category_image;
    }

    public void setCategory_image(String category_image) {
        this.category_image = category_image;
    }
    public String getCategory_description() {
        return category_description;
    }

    public void setCategory_description(String category_description) {
        this.category_description = category_description;
    }

    public String getCategory_name_tamil() {
        return category_name_tamil;
    }

    public void setCategory_name_tamil(String category_name_tamil) {
        this.category_name_tamil = category_name_tamil;
    }
    public String getSub_category_name_sinhala() {
        return sub_category_name_sinhala;
    }

    public void setSub_category_name_sinhala(String sub_category_name_sinhala) {
        this.sub_category_name_sinhala = sub_category_name_sinhala;
    }
    public String getSub_category_name_tamil() {
        return sub_category_name_tamil;
    }

    public void setSub_category_name_tamil(String sub_category_name_tamil) {
        this.sub_category_name_tamil = sub_category_name_tamil;
    }

    public String getSub_category_name_english() {
        return sub_category_name_english;
    }

    public void setSub_category_name_english(String sub_category_name_english) {
        this.sub_category_name_english = sub_category_name_english;
    }
    public String getContact_value_english() {
        return contact_value_english;
    }

    public void setContact_value_english(String contact_value_english) {
        this.contact_value_english = contact_value_english;
    }

    public int getSub_category_is_active() {
        return sub_category_is_active;
    }

    public void setSub_category_is_active(int sub_category_is_active) {
        this.sub_category_is_active = sub_category_is_active;
    }

    public String getSub_category_description() {
        return sub_category_description;
    }

    public void setSub_category_description(String sub_category_description) {
        this.sub_category_description = sub_category_description;
    }

    public String getCategory_name_sinhala() {
        return category_name_sinhala;
    }
    public void setCategory_name_sinhala(String category_name_sinhala) {
        this.category_name_sinhala = category_name_sinhala;
    }
    public String getCategory_name_english() {
        return category_name_english;
    }
    public void setCategory_name_english(String category_name_english) {
        this.category_name_english = category_name_english;
    }
}
