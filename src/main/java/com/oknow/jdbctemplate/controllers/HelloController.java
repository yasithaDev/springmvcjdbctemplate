/*
 http://www.technicalkeeda.com/spring-tutorials/spring-4-jdbctemplate-annotation-example
 */
package com.oknow.jdbctemplate.controllers;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.oknow.jdbctemplate.Daos.ItemDao;
import com.oknow.jdbctemplate.entities.Item;

@RestController
@RequestMapping("/")
public class HelloController 
{
	
    @Autowired
    private ItemDao itemDao;
    
	@RequestMapping(value = "/hello", method = RequestMethod.GET)
	public String sayHello() 
	{
		return "helloworld";
	}
    @RequestMapping(value="/allitems", method = RequestMethod.POST)
	public List<Item> getAllitems(@RequestParam("category_id") int category_id)
    {
        try {
            return  itemDao.getAllItems(category_id);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
  
}
